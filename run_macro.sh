#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
PROJECTDIR="/afs/cern.ch/work/m/mmurin/private/4top/test-4top-root-cmake"
cd $PROJECTDIR
source setup_each_time.sh
#setupATLAS --quiet
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
#lsetup python
#export LD_LIBRARY_PATH=/afs/cern.ch/work/m/mmurin/public/ExRootAnalysis:$LD_LIBRARY_PATH
root -b -q -l 'run_macro.C("control", "alpha")'
