# Code description
This is a ROOT macro for creating plots of observables from event data in LHE format. The primary use of the macro is to study sensitivity of the observables to CP property and strenght of the top-Yukawa coupling in the production of four top quarks.
The event data is usually obtained from MadGraph simulation. The paths to the data files are defined in *TChainsMaker*. The plotting is implemented in the base class *GeneralPlot* from which all observables are derived (see *Observables.cxx*).

# Setup
Required dependencies:
- ROOT
- ExRootAnalysis

On lxplus (ATLAS) `source setup_each_time.sh`

Run macro with `root -l run_macro.C`

## Important considerations
Several changes to the source code may be needed as it is not designed to run out of the box.
- Path to ExRootAnalysis need to be updated in `setup_each_time.sh` and in `run_macro.c` 
- Edit paths to event data in *TChainsMaker*
- If using this macro for any other purpose than Yukawa coupling studies change the chain names and sample sets accordingly


# Credit
Primary developer and contact: Martin Murin (martin.murin@cern.ch)
