import os, sys, glob, argparse, logging


def header(title, author, date, margin):
    s = "\
    \\documentclass{{article}}\n\
    \\usepackage[utf8]{{inputenc}}\n\
    \\usepackage{{graphicx}}\n\
    \\usepackage[margin={margin}cm]{{geometry}}\n\
    \\usepackage{{float}}\n\
    \n\
    \\title{{{title}}}\n\
    \\author{{{auth}}}\n\
    \\date{{{date}}}\n\
    ".format(margin=margin, title=title, auth=author, date=date)
    return s

def beginDoc():
    s = "\
    \\begin{{document}}\n\
    \\maketitle\n\
    ".format()
    return s

def section(sec):
    s = "\
    \\section{{{}}}\n\
    ".format(sec)
    return s


def endDoc():
    s = "\
    \\end{{document}}\
    ".format()
    return s

def makedocument(outfile, confDict, plots):
    f = open(outfile, 'w')
    f.write(header(confDict["title"], confDict["author"], confDict["date"], confDict["margin"]))
    f.write(beginDoc())
    f.write(section("Test"))
    
    for i in range(len(plots[0])):

        f.write("\\begin{{figure}}[{opt}]\n".format(opt="H"))
        f.write("\t\\centering\n")

        for j in range(len(plots)):
            f.write("\t\\includegraphics[width={:f}\\textwidth]{{{graphics}}}\n".format(1.0/len(plots)-0.02, graphics=plots[j][i]))

        f.write("\t\\caption{{{cap}}}\n".format(cap="Plots for Higgs only (left) and inclusive (right) sample set."))
        f.write("\t\\label{{fig:label{i}}}\n".format(i=i))
        f.write("\\end{figure}\n")

    f.write(endDoc())


if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description='Create plot book from directory')
    parser.add_argument('indir',                                                                      help = 'Input directory' )
    parser.add_argument('-o', '--outfile',                                                            help = 'Output file') 
    parser.add_argument('-s', '--sampleset',  choices=['higgs', 'control', 'validation'], nargs="+",  help = 'Choose samples : higgs, inclusive or validation')
    parser.add_argument('-y', '--yukawaset',  choices=['kappa','alpha'],                  nargs="+",  help = 'Choose coupling parameter : kappa or alpha')
    args = parser.parse_args()

    confDict = {
            "title" : "Top-Yukawa CP plots",
            "author" : "Martin Murin",
            "date" : "20th April 2022",
            "margin" : 2
            }

    fileList = sorted(glob.glob(args.indir + '/*pdf'))

    outfile = ""
    if args.outfile == None:
        outfile = "plots/plotbook_default.tex"
    else:
        outfile = args.outfile

    plots = []
    print(args.sampleset)
    
    for yukawa in args.yukawaset:
        if 'higgs' in args.sampleset:
            plots.append([f for f in fileList if ("MGLO_Higgs" in f and yukawa in f)])

        if 'control' in args.sampleset:
            plots.append([f for f in fileList if ('MGLO_Inclusive' in f and yukawa in f)])

        if 'validation' in args.sampleset:
            plots.append([f for f in fileList if ('Validation' in f and yukawa in f)])

        print(plots)
        makedocument(outfile, confDict, plots)
        plots = []
















